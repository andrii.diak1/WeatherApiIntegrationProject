/*
* In order to succeed it is necessary to install plugins below
* */

const Promise = require('bluebird');
const cmd = require('node-cmd');

const getAsync = Promise.promisify(cmd.get, { multiArgs: true, context: cmd });
const defaultOrgNamePrefix = 'WeatherApiIntegration';
let orgName = process.argv[2];
var alias = '';
var userEmail = ``;
var branchName = ``;

const commands = {
    GETTING_USER_EMAIL: `git config --get user.email`,
    GETTING_BRANCH_NAME: `git rev-parse --abbrev-ref HEAD`,
    ORG_CREATION: `sfdx force:org:create -f config/project-scratch-def.json -s`,
    PUSHING_METADATA: `sfdx force:source:push`,
    CREATE_INTEGRATION_DATA: `sfdx force:apex:execute -f scripts/apex/populateWeatherApiKey.apex`,
    OPEN_ORG: `sfdx force:org:open`
};

getAsync(commands.GETTING_USER_EMAIL).then(result => {
    if(result && result[0]){
        userEmail = result[0].trim();
    }
    return getAsync(commands.GETTING_BRANCH_NAME);
})
.then(result => {
    if(result && result[0]){
      branchName = result[0].trim().substring(0, 80);
    }

    let adminEmail = userEmail ? ` adminEmail=${userEmail}` : ``;
    let orgName = branchName ? ` orgName="${branchName}"` : ``;
    let alias = branchName ? ` -a "${defaultOrgNamePrefix + '-' +branchName}"` : ``;
    log(`Creating scratch org...`);
    return getAsync(commands.ORG_CREATION + adminEmail + orgName + alias);
}).then(() => {
    log(`Pushing to the org...`);
    return getAsync(commands.PUSHING_METADATA);
}).then(() => {
    log(`Executing configuration step...`);
    return getAsync(commands.CREATE_INTEGRATION_DATA);
}).then(() => {
    log(`Opening org...`);
    return getAsync(commands.OPEN_ORG);
}).then(() => {
    log(`Enjoy your new scratch org :)`);
}).catch(errors => {
    error(errors);
});

function log(message) {
    console.log(message);
}

function error(message) {
    console.error(message);
}
