# How to run this project:

## Requirement

1. You should have installed nodejs and npm in your system.
2. You should have set up DevHub with with available scratch org limits.

## Steps:

```
git clone git@gitlab.com:andrii.diak1/WeatherApiIntegrationProject.git
cd WeatherApiIntegrationProject
```
Open the file 'WeatherApiIntegrationProject/scripts/apex/populateWeatherApiKey.apex' and fill in the 'API_KEY' variable with your required integration key.
```
npm update
npm run configure-org
```

After all these steps, you will have an almost fully prepared scratch org. You only need to perform one manual step, which is changing the user's location to the English (United States). This is a known bug in SFDX that has not been fixed yet, unfortunately, for my language.

- [Link To Issue](https://github.com/forcedotcom/cli/issues/1962)
