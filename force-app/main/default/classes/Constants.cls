public with sharing class Constants {

    public static final String endpointURL = 'http://api.weatherapi.com/v1/forecast.json?key={key}&q={zipCode}&days={days}';
    public static final String KEY_URL_PLACEHOLDER = '{key}';
    public static final String ZIP_CODE_URL_PLACEHOLDER = '{zipCode}';
    public static final String DAYS_URL_PLACEHOLDER = '{days}';

    public static final String REST_METHOD_GET = 'GET';
}