public with sharing class GetForecastController {

    @AuraEnabled
    public static String getForecastByZipCode(String zipCode) {
        try {
            return GetForecastService.instance.getForecastByZipCode(zipCode);
        } catch (Exception e) {
            throw new AuraHandledException(e.getMessage());
        }
    }
}