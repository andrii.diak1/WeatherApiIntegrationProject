@IsTest
private class GetForecastControllerTest {

    private static String RESPONSE_BODY_POSITIVE = '{ "message": "positive execution"}';
    private static String RESPONSE_TEXT_NEGATIVE = 'wrong input.';

    @IsTest
    static void testPositive() {
        createCustomSetting();
        Test.setMock(HttpCalloutMock.class, new TestCalloutMock());

        String response = '';
        Test.startTest();
            response = GetForecastController.getForecastByZipCode('94105');
        Test.stopTest();

        System.assertEquals(RESPONSE_BODY_POSITIVE, response);
    }

    @IsTest
    static void testNegativeKeyNotSet() {
        Boolean exceptionWasThrown = false;

        Test.startTest();
            try {
                GetForecastController.getForecastByZipCode('94105');
            } catch (Exception e) {
                exceptionWasThrown = true;
            }
        Test.stopTest();

        System.assert(exceptionWasThrown, 'Exception was not thrown.');
    }

    @IsTest
    static void testNegativeBadInput() {
        createCustomSetting();

        Boolean exceptionWasThrown = false;
        String exceptionMessage = '';

        Test.setMock(HttpCalloutMock.class, new TestCalloutMock(JSON.serialize(
            new GetForecastService.ErrorDTO(new GetForecastService.ErrorInfoDTO(RESPONSE_TEXT_NEGATIVE))
        ) , 400));

        Test.startTest();
            try {
                GetForecastController.getForecastByZipCode('sdfkmdsfio343');
            } catch (Exception e) {
                exceptionWasThrown = true;
                exceptionMessage = e.getMessage();
            }
        Test.stopTest();

        System.assert(exceptionWasThrown, 'Exception was not thrown.');
        System.assertEquals('Script-thrown exception', exceptionMessage, 'Exception message should be the same');
    }

    static void createCustomSetting() {
        insert new Weather_API_Integration__c(Key__c = 'TestKey');
    }

    public class TestCalloutMock implements HttpCalloutMock {

        public String DEFAULT_RESPONSE_BODY = RESPONSE_BODY_POSITIVE;
        public Integer DEFAULT_RESPONSE_STATUS_CODE = 200;
        private String body;
        private Integer statusCode;

        public TestCalloutMock() { }

        public TestCalloutMock(String body) {
            this.body = body;
        }

        public TestCalloutMock(String body, Integer statusCode) {
            this.body = body;
            this.statusCode = statusCode;
        }

        public HttpResponse respond(HttpRequest request) {
            HttpResponse response = new HttpResponse();
            response.setHeader('Content-Type', 'application/json');
            response.setBody(getBody());
            response.setStatusCode(getStatusCode());
            return response;
        }

        private String getBody() {
            return String.isNotEmpty(body) ? body : DEFAULT_RESPONSE_BODY;
        }

        private Integer getStatusCode() {
            return String.isNotEmpty(String.valueOf(statusCode)) ? statusCode : DEFAULT_RESPONSE_STATUS_CODE;
        }
    }
}