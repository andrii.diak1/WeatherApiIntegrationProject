public with sharing class GetForecastService {

    private static final String DEFAULT_NUMBER_OF_DAYS = '5';

    public static GetForecastService instance {
        get {
            if (instance == null) {
                instance = new GetForecastService();
            }

            return instance;
        }

        private set;
    }

    private GetForecastService() { }

    public String getForecastByZipCode(String zipCode) {
        String apiKey = getApiKey();
        String endpointURL = Constants.endpointURL;

        if (String.isEmpty(apiKey)) {
            throw new GetForecastServiceException(System.Label.Integration_API_Code_IS_Not_Configured);
        }

        Http http = new Http();
        HttpRequest request = new HttpRequest();
        HttpResponse response;

        request.setEndpoint(
            endpointURL.replace(Constants.KEY_URL_PLACEHOLDER, apiKey)
                .replace(Constants.ZIP_CODE_URL_PLACEHOLDER, zipCode)
                .replace(Constants.DAYS_URL_PLACEHOLDER, DEFAULT_NUMBER_OF_DAYS));
        request.setMethod(Constants.REST_METHOD_GET);

        response = http.send(request);

        if (response.getStatusCode() == 200) {
            return response.getBody();
        } else {
            ErrorDTO errorValue = (ErrorDTO) JSON.deserialize(response.getBody(), ErrorDTO.class);
            throw new GetForecastServiceException(errorValue.error.message);
        }
    }

    private String getApiKey() {
        return Weather_API_Integration__c.getInstance().Key__c;
    }

    public class ErrorDTO {

        @AuraEnabled
        public ErrorInfoDTO error;

        public ErrorDTO(ErrorInfoDTO error) {
            this.error = error;
        }
    }

    public class ErrorInfoDTO {

        @AuraEnabled
        public String message;

        public ErrorInfoDTO(String message) {
            this.message = message;
        }
    }

    public class GetForecastServiceException extends Exception {}
}