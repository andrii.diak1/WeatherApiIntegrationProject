import {LightningElement, api} from 'lwc';

export default class ForecastResponseView extends LightningElement {

    @api dailyForecasts = [];
    @api temperatureScale = '';

}