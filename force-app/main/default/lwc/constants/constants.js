import celsiusFull from '@salesforce/label/c.Celsius_Full';
import fahrenheitFull from '@salesforce/label/c.Fahrenheit_Full';
import selectedLocationText from '@salesforce/label/c.Selected_Location_Text';
import zipCodeInputTextLabel from '@salesforce/label/c.Zip_Code_Input_Text_Label';
import zipCodeInputPlaceholder from '@salesforce/label/c.Zip_Code_Input_Placeholder';
import weatherHomePageHeader from '@salesforce/label/c.Weather_Home_Page_Header';
import responsePlaceholder from '@salesforce/label/c.Response_Placeholder';
import errorBoxTextPrefix from '@salesforce/label/c.Error_Box_Text_Prefix';
import maxTemperature from '@salesforce/label/c.Max_Temperature';
import minTemperature from '@salesforce/label/c.Min_Temperature';
import celsiusSmall from '@salesforce/label/c.Celsius_Small';
import fahrenheitSmall from '@salesforce/label/c.Fahrenheit_Small';

const TEMPERATURE_SCALE_CELSIUS = 'celsius';
const TEMPERATURE_SCALE_FAHRENHEIT = 'fahrenheit';

const label = {
    celsiusFull,
    selectedLocationText,
    zipCodeInputTextLabel,
    fahrenheitFull,
    zipCodeInputPlaceholder,
    weatherHomePageHeader,
    responsePlaceholder,
    errorBoxTextPrefix,
    maxTemperature,
    minTemperature,
    celsiusSmall,
    fahrenheitSmall
};

export {label, TEMPERATURE_SCALE_CELSIUS, TEMPERATURE_SCALE_FAHRENHEIT};