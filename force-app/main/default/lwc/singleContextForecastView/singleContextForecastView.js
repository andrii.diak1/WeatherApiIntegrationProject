import {LightningElement, api, track} from 'lwc';
import {label as labels} from 'c/constants';
import {TEMPERATURE_SCALE_CELSIUS, TEMPERATURE_SCALE_FAHRENHEIT} from 'c/constants';

export default class SingleContextForecastView extends LightningElement {

    @api date;
    @api day;
    @api temperatureScale = '';

    @track labels;

    constructor() {
        super();
        this.labels = labels;
    }


    get isCelsius() {
        return this.temperatureScale === TEMPERATURE_SCALE_CELSIUS;
    }

    get isFahrenheit() {
        return this.temperatureScale === TEMPERATURE_SCALE_FAHRENHEIT;
    }
}