import {LightningElement, track} from 'lwc';
import getForecastByZipCode from '@salesforce/apex/GetForecastController.getForecastByZipCode';
import {label as labels} from 'c/constants';
import {TEMPERATURE_SCALE_CELSIUS, TEMPERATURE_SCALE_FAHRENHEIT} from 'c/constants';

export default class GetForecastPage extends LightningElement {

    @track isSpinnerLoading = false;
    @track zipCode;
    @track errorValue = '';
    @track dailyForecasts = [];
    @track temperatureScale = TEMPERATURE_SCALE_CELSIUS;
    @track labels;

    constructor() {
        super();
        this.labels = labels;
    }

    onZipCodeFieldChange(event) {
        this.zipCode = event.target.value;
    }

    onTemperatureScaleChange(event) {
        this.temperatureScale = event.target.value;
    }

    handleKeyUp(evt) {
        const isEnterKey = evt.keyCode === 13;

        if (isEnterKey) {
            this.handleGetForecast();
        }
    }

    handleGetForecast() {
        this.isSpinnerLoading = true;

        getForecastByZipCode({
            zipCode: this.zipCode
        }).then(result => {
            this.errorValue = '';
            this.apiResponse = JSON.parse(result);
            this.dailyForecasts = this.apiResponse?.forecast?.forecastday;
        }).catch(error => {
            this.apiResponse = null;
            this.dailyForecasts = [];
            this.errorValue = error?.body?.message;
        }).finally(() => {
            this.isSpinnerLoading = false;
        });
    }

    get isGetForecastButtonDisabled() {
        return !this.zipCode;
    }

    get isDailyForecastExist() {
        return this.dailyForecasts && this.dailyForecasts.length > 0;
    }

    get temperatureScaleOptions() {
        return [
            { label: this.labels.celsiusFull, value: TEMPERATURE_SCALE_CELSIUS },
            { label: this.labels.fahrenheitFull, value: TEMPERATURE_SCALE_FAHRENHEIT }
        ];
    }

    get selectedPlace() {
        if (!this.apiResponse) {
            return '';
        }

        let location = this.apiResponse?.location;
        return this.labels.selectedLocationText + ' ' + location.name + ' ' + location.region + ' ' + location.country;
    }

    get isErrorHappened() {
        return !!this.errorValue;
    }
}